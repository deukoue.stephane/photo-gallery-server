package com.example.cosee.repositories;

import com.example.cosee.models.Category;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Deukoue Stephane
 * @since 12/13/2021
 */
@DataJpaTest
public class CategoryRepositoryTest {
    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    void findCategoryByNameExist() {
        // given
        Category category = createCategory();

        // when
        Optional<Category> expectedCategory = categoryRepository.findByName(category.getName());

        // then
        assert expectedCategory.isPresent();
        assert expectedCategory.get().getName().equals(category.getName());
        assert expectedCategory.get().getCategoryId().equals(category.getCategoryId());
        assert expectedCategory.get().getCreatedAt() == category.getCreatedAt();
    }

    @Test
    void findCategoryByNameDoesNotExist() {
        Optional<Category> expectedCategory = categoryRepository.findByName("anything");

        assert !expectedCategory.isPresent();
    }

    private Category createCategory() {
        Category category = new Category(
                UUID.randomUUID().toString(),
                "Category test",
                Instant.now()
        );
        categoryRepository.save(category);

        return category;
    }
}
