package com.example.cosee.service;

import com.example.cosee.models.Category;
import com.example.cosee.repositories.CategoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.UUID;

import static org.mockito.Mockito.verify;

/**
 * @author Deukoue Stephane
 * @since 12/14/2021
 */
@ExtendWith(MockitoExtension.class)
class CategoryServiceImplementationTest {

    @Mock
    private CategoryRepository categoryRepository;
    private CategoryServiceImplementation categoryService;

    @BeforeEach
    void setUp() {
        categoryService = new CategoryServiceImplementation(categoryRepository);
    }

    @Test
    void saveCategory() {
        // given
        Category category = createCategory();

        // when
        categoryService.saveCategory(category);

        // then
        ArgumentCaptor<Category> categoryArgumentCaptor = ArgumentCaptor.forClass(Category.class);
        verify(categoryRepository).save(categoryArgumentCaptor.capture());
        Category expectedCategory = categoryArgumentCaptor.getValue();
        assert expectedCategory.equals(category);
    }

    @Test
    void getAllCategories() {
        // when
        categoryService.getAllCategories();
        // then
        verify(categoryRepository).findAll();
    }

    @Test
    void getCategoryById() {
        // given
        Category category = createCategory();

        // when
        categoryService.getCategoryById(category.getCategoryId());

        // then
        ArgumentCaptor<String> categoryArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(categoryRepository).findById(categoryArgumentCaptor.capture());
        String expectedCategoryId = categoryArgumentCaptor.getValue();
        assert expectedCategoryId.equals(category.getCategoryId());
    }

    @Test
    void getCategoryByName() {
        // given
        Category category = createCategory();

        // when
        categoryService.getCategoryByName(category.getName());

        // then
        ArgumentCaptor<String> categoryArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(categoryRepository).findByName(categoryArgumentCaptor.capture());
        String expectedCategoryName = categoryArgumentCaptor.getValue();
        assert expectedCategoryName.equals(category.getName());
    }

    @Test
    void deleteById() {
        // given
        Category category = createCategory();

        // when
        categoryService.deleteById(category.getCategoryId());

        // then
        ArgumentCaptor<String> categoryArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(categoryRepository).deleteById(categoryArgumentCaptor.capture());
        String expectedCategoryId = categoryArgumentCaptor.getValue();
        assert expectedCategoryId.equals(category.getCategoryId());
    }

    private Category createCategory() {
        return new Category(
                UUID.randomUUID().toString(),
                "Test",
                Instant.now()
        );
    }
}
