package com.example.cosee.controllers;

import com.example.cosee.dto.CategoryBodyRequest;
import com.example.cosee.excecptions.CategoryExistException;
import com.example.cosee.models.Category;
import com.example.cosee.repositories.CategoryRepository;
import com.example.cosee.service.CategoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.UUID;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Deukoue Stephane
 * @since 12/14/2021
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@TestPropertySource(locations = "classpath:application.properties")
class CategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private CategoryService categoryService;
    @Autowired
    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    @Disabled
    void getAllCategories() throws Exception {
        Category category = new Category(
                UUID.randomUUID().toString(),
                "Test",
                Instant.now()
        );

        // categoryRepository.save(category);

        when(categoryRepository.save(category)).thenReturn(category);

        mockMvc.perform(get("/api/categories/").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$.[0].name", hasItems("Test")));
    }

    @Test
    void addNewCategoryShouldSucceed() throws Exception {
        CategoryBodyRequest request = new CategoryBodyRequest("test");

        Category category = new Category(
                UUID.randomUUID().toString(),
                request.getName(),
                Instant.now()
        );

        when(categoryRepository.save(category)).thenReturn(category);

        mockMvc.perform(post("/api/categories/add/")
                        .content(objectMapper.writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").value(true));
    }

    @Test
    @Disabled
    void addNewCategoryShouldFailed() throws Exception {
        CategoryBodyRequest request = new CategoryBodyRequest("test");

        Category category = new Category(
                UUID.randomUUID().toString(),
                request.getName(),
                Instant.now()
        );
        categoryRepository.save(category);
        when(categoryRepository.save(category)).thenReturn(category);

        mockMvc.perform(post("/api/categories/add/")
                        .content(objectMapper.writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON)
                ).andDo(print())
                .andExpect(status().isNotAcceptable())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof CategoryExistException))
                .andExpect(result -> assertEquals(
                        "The provided category already exist.",
                        result.getResolvedException().getMessage()
                ));
    }


    @Test
    @Disabled
    void updateCategory() {
    }

    @Test
    @Disabled
    void deleteCategory() {
    }

    @Test
    @Disabled
    void deleteCategoryPicture() {
    }

    @Test
    @Disabled
    void uploadPicture() {
    }

    private void createCategory() {
        Category category = new Category(
                UUID.randomUUID().toString(),
                "Test",
                Instant.now()
        );
        // categoryRepository.save(category);
    }
}
