package com.example.cosee.service;

import com.example.cosee.models.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    void saveCategory(Category category);
    List<Category> getAllCategories();
    Optional<Category> getCategoryById(String categoryId);
    Optional<Category> getCategoryByName(String name);
    void deleteById(String categoryId);
}
