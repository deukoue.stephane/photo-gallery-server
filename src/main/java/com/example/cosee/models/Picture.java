package com.example.cosee.models;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Arrays;

@Data
@Entity
@Table(name = "picture")
public class Picture {
    @Id
    @Size(max = 36)
    @Column(unique = true)
    @NotNull
    private String pictureId;

    @NonNull
    private byte[] file;

    @NonNull
    private Instant createdAt;

    public Picture() { }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "pictureId='" + pictureId + '\'' +
                ", file=" + Arrays.toString(file) +
                ", createdAt=" + createdAt +
                '}';
    }
}
