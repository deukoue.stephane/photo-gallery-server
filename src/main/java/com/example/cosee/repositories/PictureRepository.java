package com.example.cosee.repositories;

import com.example.cosee.models.Picture;
import org.springframework.data.repository.CrudRepository;

public interface PictureRepository extends CrudRepository<Picture, String> {
    // List<Picture> findByCategory(Category category, Sort sort);
}
