package com.example.cosee.repositories;

import com.example.cosee.models.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public interface CategoryRepository extends CrudRepository<Category, String> {
    Optional<Category> findByName(String name);
}
