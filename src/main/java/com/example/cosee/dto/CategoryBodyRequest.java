package com.example.cosee.dto;

public class CategoryBodyRequest {
    private final String name;

    public CategoryBodyRequest() {
        this.name = "";
    }
    public CategoryBodyRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
