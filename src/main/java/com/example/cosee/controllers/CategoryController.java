package com.example.cosee.controllers;

import com.example.cosee.dto.CategoryBodyRequest;
import com.example.cosee.excecptions.CategoryExistException;
import com.example.cosee.excecptions.CategoryNotFoundException;
import com.example.cosee.models.Category;
import com.example.cosee.models.Picture;
import com.example.cosee.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/categories")
@CrossOrigin(origins = "http://localhost:4200")
public class CategoryController {

    private final CategoryService categoryService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * Get the list of all categories present in the database
     *
     * @return Category[]
     */
    @GetMapping("/")
    public List<Category> getAllCategories() {
        return categoryService.getAllCategories().stream().sorted().collect(Collectors.toList());
    }

    /**
     * Creates new category
     *
     * @param givenCategory holds the name of the new category
     * @return ResponseEntity<Boolean>
     */
    @PostMapping("/add")
    public ResponseEntity<Boolean> addNewCategory(@Valid @RequestBody CategoryBodyRequest givenCategory) {
        if (categoryExist(givenCategory.getName())) {
            throw new CategoryExistException("The provided category already exist.");
        }

        Category category = new Category(
                UUID.randomUUID().toString(),
                givenCategory.getName(),
                Instant.now()
        );

        categoryService.saveCategory(category);

        return ResponseEntity.status(200).body(true);
    }

    /**
     * Update a category
     *
     * @param categoryId    the id of the category
     * @param givenCategory holds the name of the new category
     * @return ResponseEntity<Boolean>
     */
    @PostMapping("/update/{categoryId}")
    public ResponseEntity<Category> updateCategory(
            @PathVariable(value = "categoryId") UUID categoryId,
            @RequestBody CategoryBodyRequest givenCategory
    ) {
        Optional<Category> category = categoryService.getCategoryById(categoryId.toString());

        if (category.isPresent()) {
            category.get().setName(givenCategory.getName());

            categoryService.saveCategory(category.get());

            return ResponseEntity.ok(category.get());
        } else {
            throw new CategoryNotFoundException("The provided category does not exist.");
        }
    }

    /**
     * Delete a category
     *
     * @param categoryId the id of the category
     * @return ResponseEntity<Boolean>
     */
    @DeleteMapping("/{categoryId}")
    public ResponseEntity<Boolean> deleteCategory(@PathVariable(value = "categoryId") UUID categoryId) {
        Optional<Category> category = categoryService.getCategoryById(categoryId.toString());
        if (category.isPresent()) {
            categoryService.deleteById(categoryId.toString());

            return ResponseEntity.ok(true);
        } else {
            throw new CategoryNotFoundException("The provided category does not exist.");
        }
    }

    /**
     * Delete pictures of a category
     *
     * @param categoryId the id of the category
     * @param pictureId  the id of the picture to delete
     * @return ResponseEntity<Boolean>
     */
    @DeleteMapping("/picture/{categoryId}/{pictureId}")
    public ResponseEntity<Boolean> deleteCategoryPicture(@PathVariable(value = "categoryId") UUID categoryId, @PathVariable(value = "pictureId") UUID pictureId) {
        Optional<Category> category = categoryService.getCategoryById(categoryId.toString());
        if (category.isPresent()) {
            category.get().getPictures().removeIf(picture -> picture.getPictureId().equals(pictureId.toString()));
            categoryService.saveCategory(category.get());

            return ResponseEntity.ok(true);
        } else {
            throw new CategoryNotFoundException("The provided category does not exist.");
        }
    }

    /**
     * Assign a category new pictures
     *
     * @param categoryId the id of the category
     * @param files      the list of pictures to save
     * @return ResponseEntity<Boolean>
     */
    @PostMapping("/upload")
    public ResponseEntity<Boolean> uploadPicture(
            @RequestParam("files") List<MultipartFile> files,
            @RequestParam("categoryId") UUID categoryId) {
        log.info("Category ID: " + categoryId);
        log.info("files " + files);
        Optional<Category> category = categoryService.getCategoryById(categoryId.toString());

        if (category.isPresent()) {
            files.forEach(file -> {
                Picture picture = new Picture();
                picture.setPictureId(UUID.randomUUID().toString());
                try {
                    picture.setFile(file.getBytes());
                } catch (IOException e) {
                    log.error("Error while tying to get the file bytes" + e.getMessage());
                }
                picture.setCreatedAt(Instant.now());

                category.get().getPictures().add(picture);
            });

            categoryService.saveCategory(category.get());

            return ResponseEntity.status(200).body(true);
        } else {
            throw new CategoryNotFoundException("The provided category does not exist.");
        }
    }

    private boolean categoryExist(String name) {
        return categoryService.getCategoryByName(name).isPresent();
    }

}
