FROM gradle:7.3.2-jdk8 AS gradle-build
COPY . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build

FROM openjdk:8
EXPOSE 8081
RUN mkdir -p /home/server
COPY --from=gradle-build /home/gradle/src/build/libs/*.jar /home/server/spring-boot-server.jar
ENTRYPOINT ["java", "-jar", "/home/server/spring-boot-server.jar"]
